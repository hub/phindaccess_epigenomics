# PHINDaccess_epigenomics

## Material for practicals

Biological material & question: 
Mouse embryonic fibroblasts (MEF), in the process of reprogrammation into iPSC (induced pluripotent stem cells).

How is the reprogrammation ( = erasure of fibroblastic programme and acquisition of pluripotency programme) influenced by a global reduction in SUMO levels ? 

**Datasets used for practical:** 

- H3K27ac (marking active CRMs), Klf4 (pluripotency-specific TF) 
- ChIPseq ; ATACseq 
- acquired on MEFs after 4 days of reprogrammation in two conditions, SUMO KD (shUdc9) and control (shScrambled)
- 2 replicates each
- Data has be processed to match only to Mouse chromosome 1

## Data

Data used during all Hands-on come from this study: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE99009


**ChIP-seq Hands-On:**

```
GSM2629955 SRR5572646	ChIP-seq_H3K27ac_D4_shCtrl_rep1
GSM2629956 SRR5572647	ChIP-seq_H3K27ac_D4_shCtrl_rep2
GSM2629961 SRR5572652	ChIP-seq_INPUT_D4_shCtrl_rep1
GSM2629962 SRR5572653	ChIP-seq_INPUT_D4_shCtrl_rep2
GSM2629967 SRR5572658	ChIP-seq_H3K27ac_D4_shUbc9_rep1
GSM2629968 SRR5572659	ChIP-seq_H3K27ac_D4_shUbc9_rep2
GSM2629973 SRR5572664	ChIP-seq_INPUT_D4_shUbc9_rep1
GSM2629974 SRR5572665	ChIP-seq_INPUT_D4_shUbc9_rep2
GSM2629977 SRR5572668	ChIP-seq_Klf4_D4_shCtrl_rep1
GSM2629978 SRR5572669	ChIP-seq_Klf4_D4_shCtrl_rep2
GSM2629985 SRR5572676	ChIP-seq_Klf4_D4_shUbc9_rep1
GSM2629986 SRR5572677	ChIP-seq_Klf4_D4_shUbc9_rep2
```



**ATAC-seq Hands-On:**

```
GSM2629991 SRR5572682	ATAC-seq_D4_shCtrl_rep1
GSM2629992 SRR5572684	ATAC-seq_D4_shCtrl_rep2
GSM2629993 SRR5572686	ATAC-seq_D4_shCtrl_rep3
GSM2629994 SRR5572688 	ATAC-seq_D4_shUbc9_rep1
GSM2629995 SRR5572690	ATAC-seq_D4_shUbc9_rep2
GSM2629996 SRR5572692	ATAC-seq_D4_shUbc9_rep3
```

## Hands-on programme

| Course    | training        | 
|-----------|--------------------|
| ChIPseq: QC metrics | [Hands-on](Monday/ePeak_QC_metrics.md)  |
| ChIPseq: Peak calling | [Hands-on](Tuesday/ePeak_PeakCalling.md)  |
| Visualization: IGV | [Hands-on](Tuesday/IGV.md)  |
| Visualization: Deeptools | [Hands-on](Tuesday/Deeptools.md)  |
| ATACseq: analysis | [Hands-on](Wednesday/ATACflow.md)  |
| ChIPseq: Differential binding | [Hands-on](Thursday/chipflowR.md)  |
| ChIPseq: Peak annotation | [Hands-on](Thursday/peak_annotation.Rmd )  |
| ChIPseq: Motif finding | [Hands-on](Thursday/MEME.md)  |



## Friday session : questions 
Feel free to add your questions here :
https://docs.google.com/document/d/1MjZz2wyQqiZbSD386N6xOYZvPDgibRvBerXzbgKRJp8/edit?usp=sharing


## Trainers 

- Claudia Chica: <claudia.chica@pasteur.fr> 
- Victoire Baillet: <victoire.baillet@pasteur.fr>
- Rachel Legendre: <rachel.legendre@pasteur.fr> 
- Hélène Lopez-Maestre: <helene.lopez-maestre@pasteur.fr>

