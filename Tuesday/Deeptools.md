# Hands-On: Visualization using Deeptools - Genomic features enrichment and metagenes

DeepTools: tools for exploring sequencing data : https://deeptools.readthedocs.io/en/develop/

*deepTools addresses the challenge of handling the large amounts of data that are now routinely generated from DNA sequencing centers. deepTools contains useful modules to process the mapped reads data for multiple quality checks, creating normalized coverage files in standard bedGraph and bigWig file formats, that allow comparison between different files (for example, treatment and control). Finally, using such normalized and standardized files, deepTools can create many publication-ready visualizations to identify enrichments and for functional annotations of the genome.*

# Correlation plot

Tool for the analysis and visualization of sample correlations.

To generate the correlation plot there are 3 steps : 
- first we need to generate for each sample a bigwig file containing the info about the (normalized) coverage of the genome per bin, these files can be generated with the **bamCoverage** function of Deeptools
- then an intermediary file is generated, summarising the info about all the samples we want to analyze with **multiBigwigSummary**
- finaly calculate the correlation between all samples and plot the graphic with **plotCorrelation**

### Bam Coverage
The first step is to generate the bw files for each sample using BamCoverage. Look at the documentation [here](https://deeptools.readthedocs.io/en/develop/content/tools/bamCoverage.html) 

This tool takes an alignment of reads or fragments as input (BAM file) and generates a coverage track (bigWig or bedGraph) as output. The coverage is calculated as the number of reads per bin, where bins are short consecutive counting windows of a defined size. It is possible to extended the length of the reads to better reflect the actual fragment length. BamCoverage offers normalization by scaling factor, Reads Per Kilobase per Million mapped reads (RPKM), counts per million (CPM), bins per million mapped reads (BPM) and 1x depth (reads per genome coverage, RPGC).
 
The bw files have already being generated by ePeak, so we don't need to do it again, but here is an example of command line for one sample :
```
bamCoverage --bam sample.bam -o sample_SeqDepthNorm.bw \
    --binSize 10
    --normalizeUsing RPGC
    --effectiveGenomeSize 2150570000
    --extendReads
```

### MultiBigwigSummary

We can then combine all the samples by running :

```
multiBigwigSummary bins --binSize 50 -b file1.bw file2.bw ... fileN.bw  -o multibwsumresults.npz
```

### Plot graphic

Look at the documentation of plotGraphic [here](https://deeptools.readthedocs.io/en/develop/content/tools/plotCorrelation.html)
```
usage: plotCorrelation [-h] --corData/-in FILE --corMethod {spearman,pearson}
                       --whatToPlot {heatmap,scatterplot} [--plotFile/-o FILE]
                       [--skipZeros]
                       [--labels sample1 sample2 [sample1 sample2 ...]]
                       [--plotTitle PLOTTITLE] [--plotFileFormat FILETYPE]
                       [--removeOutliers] [--version]
                       [--outFileCorMatrix FILE] [--plotHeight PLOTHEIGHT]
                       [--plotWidth PLOTWIDTH] [--zMin ZMIN] [--zMax ZMAX]
                       [--colorMap] [--plotNumbers] [--xRange XRANGE XRANGE]
                       [--yRange YRANGE YRANGE] [--log1p]
```
Here we are doing a correlation heatmap with spearman's method. We can save the correlation scores in a tab file with the option `--outFileCorMatrix`
```
plotCorrelation \
-in multibwsumresults.npz \
--corMethod spearman --skipZeros \
--plotTitle "Your title here" \
--whatToPlot heatmap \
-o Plot_SpearmanCorr_bigwigScores.png   \
--outFileCorMatrix SpearmanCorr_bigwigScores.tab \ 
--colorMap Blues
```

<img src="deeptools_images/Plot_SpearmanCorr_bigwigScores.png" width="700" align="center" >



# GeneBody plot (Heatmap and Profile) 
### Example with one sample

From the bw file, we use the function *computeMatrix* that prepares an intermediate file which can be used with plot functions of deeptools like *plotHeatmap*.
**ComputeMatrix** calculates scores per genome regions, like genes or user defined regions, using one (or more) BED files. 

Here we can use the file mm10_chr1_RefSeq.bed from the genome data, that contains the coordinates of the genes (and other features ?) of mm10 chr1.

```
IGVdir=/path/to/12-IGV
genome=/path/to/genome
outdir=/path/to/outdir

sample="H3K27ac_shCtrl_Rep1"            # Can be H3K27ac_shCtrl_Rep2 H3K27ac_shUbc9_Rep1 H3K27ac_shUbc9_Rep2 Klf4_shCtrl_Rep1 Klf4_shCtrl_Rep2 Klf4_shUbc9_Rep1 Klf4_shUbc9_Rep2
```

Have a look at the documentation of **ComputeMatrix** [here](https://deeptools.readthedocs.io/en/develop/content/tools/computeMatrix.html)

We see that we can run:

**computeMatrix reference-point**: Reference-point refers to a position within a BED region (e.g., the starting point). In this mode, only those genomic positions before (upstream) and/or after (downstream) of the reference point will be plotted, here is an command line example :
```
computeMatrix reference-point -S $IGVdir/${sample}_mm10_coverage.bw  -R $genome/mm10_chr1_RefSeq.bed \
--beforeRegionStartLength 3000 --afterRegionStartLength 3000 --skipZeros -o $outdir/${sample}_TSS.mat.gz -p 2
```

OR

**computeMatrix scale-regions**: all regions in the BED file are stretched or shrunken to the length (in bases) indicated by the with the option `--regionBodyLength` :
```
computeMatrix scale-regions -S $IGVdir/${sample}_mm10_coverage.bw  -R $genome/mm10_chr1_RefSeq.bed \
--beforeRegionStartLength 3000 --regionBodyLength 5000 --afterRegionStartLength 3000 --skipZeros \
-o $outdir/${sample}_scale-genes.mat.gz -p 2
```

then we can run the [plotHeatmap command](https://deeptools.readthedocs.io/en/develop/content/tools/plotHeatmap.html) : 

```
plotHeatmap -m ${sample}_scale-genes.mat.gz -out ${sample}_scale-gene.png --whatToShow 'plot and heatmap'
```

This figures are also generated by ePeak and can be found in your analysis output directory : *05-QC/GeneBody/*

<img src="deeptools_images/H3K27ac_shCtrl_Rep1_scale-genes.png" height="900" > <img src="deeptools_images/H3K27ac_shUbc9_Rep1_scale-genes.png" height="900" > <img src="deeptools_images/Klf4_shCtrl_Rep1_scale-genes.png" height="900" > <img src="deeptools_images/Klf4_shUbc9_Rep1_scale-genes.png" height="900" >


### Two or more samples

Now let's compare several samples. To do so, the counts *need to be normalized*. In our case we have already normalized the counts with RPGC when generating the bw files. 

Here we want to plot both replicates of H3K27ac_shUbc9, and compare them with H3K27ac_shCtrl.


options : 

`--sortUsingSamples` by default sort regions using the mean of both samples

`--sortUsing` Possible choices: mean, median, max, min, sum, region_length. Indicate which method should be used for sorting. The value is computed for each row.


```
computeMatrix reference-point -S $bwdir/H3K27ac_shUbc9_Rep1_mm10_coverage.bw $bwdir/H3K27ac_shUbc9_Rep2_mm10_coverage.bw \
$bwdir/H3K27ac_shCtrl_Rep1_mm10_coverage.bw $bwdir/H3K27ac_shCtrl_Rep2_mm10_coverage.bw \
--sortUsingSamples 1 2 --sortUsing mean --referencePoint TSS -R genome/mm10_chr1_RefSeq.bed \
--beforeRegionStartLength 3000 --afterRegionStartLength 3000 --skipZeros \
-o Samples_H3K27ac_shUbc9-vs-shCtrl_TSS.mat.gz -p 2
```

```
plotHeatmap -m Samples_H3K27ac_shUbc9-vs-shCtrl_TSS.mat.gz -out Samples_H3K27ac_shUbc9-vs-shCtrl_TSS.png \
--whatToShow 'plot and heatmap' \
--samplesLabel H3K27ac_shUbc9_Rep1 H3K27ac_shUbc9_Rep2 H3K27ac_shCtrl_Rep1 H3K27ac_shCtrl_Rep2 
```


<img src="deeptools_images/Samples_H3K27ac_shUbc9-vs-shCtrl_TSS.png" width="700" align="center" >


Another example were we compare H3K27ac_shUbc9 with Klf4_shUbc9 : 

<img src="deeptools_images/Samples_H3K27ac_shUbc9-vs-Klf4_shUbc9_TSS.png" width="700" align="center" >
