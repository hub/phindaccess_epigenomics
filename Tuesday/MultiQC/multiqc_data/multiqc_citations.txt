10.1093/bioinformatics/btw354                      # MultiQC
10.14806/ej.17.1.200                               # cutadapt
10.1038/nmeth.1923                                 # bowtie2
10.1038/nmeth.3317                                 # bowtie2
10.1038/s41587-019-0201-4                          # bowtie2
10.1093/nar/gkw257                                 # deepTools
10.1101/gr.136184.111                              # phantompeakqualtools
10.1038/nbt.1508                                   # phantompeakqualtools

