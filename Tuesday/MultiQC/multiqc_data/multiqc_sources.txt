Module	Section	Sample Name	Source
FastQC	all_sections	INPUT_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/INPUT_shUbc9_Rep2_R1_fastqc.zip
FastQC	all_sections	Klf4_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/Klf4_shCtrl_Rep2_R1_fastqc.zip
FastQC	all_sections	Klf4_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/Klf4_shUbc9_Rep1_R1_fastqc.zip
FastQC	all_sections	INPUT_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/INPUT_shCtrl_Rep2_R1_fastqc.zip
FastQC	all_sections	H3K27ac_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/H3K27ac_shCtrl_Rep1_R1_fastqc.zip
FastQC	all_sections	Klf4_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/Klf4_shUbc9_Rep2_R1_fastqc.zip
FastQC	all_sections	INPUT_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/INPUT_shUbc9_Rep1_R1_fastqc.zip
FastQC	all_sections	INPUT_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/INPUT_shCtrl_Rep1_R1_fastqc.zip
FastQC	all_sections	H3K27ac_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/H3K27ac_shUbc9_Rep1_R1_fastqc.zip
FastQC	all_sections	H3K27ac_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/H3K27ac_shCtrl_Rep2_R1_fastqc.zip
FastQC	all_sections	H3K27ac_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/H3K27ac_shUbc9_Rep2_R1_fastqc.zip
FastQC	all_sections	Klf4_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/00-Fastqc/Klf4_shCtrl_Rep1_R1_fastqc.zip
Cutadapt	all_sections	H3K27ac_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/H3K27ac_shUbc9_Rep1_trim.txt
Cutadapt	all_sections	H3K27ac_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/H3K27ac_shCtrl_Rep1_trim.txt
Cutadapt	all_sections	INPUT_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/INPUT_shCtrl_Rep1_trim.txt
Cutadapt	all_sections	H3K27ac_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/H3K27ac_shCtrl_Rep2_trim.txt
Cutadapt	all_sections	Klf4_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/Klf4_shUbc9_Rep1_trim.txt
Cutadapt	all_sections	Klf4_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/Klf4_shCtrl_Rep1_trim.txt
Cutadapt	all_sections	INPUT_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/INPUT_shUbc9_Rep2_trim.txt
Cutadapt	all_sections	INPUT_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/INPUT_shUbc9_Rep1_trim.txt
Cutadapt	all_sections	INPUT_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/INPUT_shCtrl_Rep2_trim.txt
Cutadapt	all_sections	Klf4_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/Klf4_shCtrl_Rep2_trim.txt
Cutadapt	all_sections	H3K27ac_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/H3K27ac_shUbc9_Rep2_trim.txt
Cutadapt	all_sections	Klf4_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/01-Trimming/logs/Klf4_shUbc9_Rep2_trim.txt
Bowtie 2 / HiSAT2	all_sections	INPUT_shUbc9_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/INPUT_shUbc9_Rep2_spikes_mapping.err
Bowtie 2 / HiSAT2	all_sections	Klf4_shUbc9_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/Klf4_shUbc9_Rep1_spikes_mapping.err
Bowtie 2 / HiSAT2	all_sections	INPUT_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/INPUT_shCtrl_Rep2_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	Klf4_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/Klf4_shCtrl_Rep1_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	Klf4_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/Klf4_shUbc9_Rep1_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	INPUT_shCtrl_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/INPUT_shCtrl_Rep1_spikes_mapping.err
Bowtie 2 / HiSAT2	all_sections	INPUT_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/INPUT_shCtrl_Rep1_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	Klf4_shUbc9_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/Klf4_shUbc9_Rep2_spikes_mapping.err
Bowtie 2 / HiSAT2	all_sections	H3K27ac_shCtrl_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/H3K27ac_shCtrl_Rep1_spikes_mapping.err
Bowtie 2 / HiSAT2	all_sections	INPUT_shCtrl_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/INPUT_shCtrl_Rep2_spikes_mapping.err
Bowtie 2 / HiSAT2	all_sections	H3K27ac_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/H3K27ac_shCtrl_Rep2_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	H3K27ac_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/H3K27ac_shUbc9_Rep1_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	H3K27ac_shUbc9_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/H3K27ac_shUbc9_Rep1_spikes_mapping.err
Bowtie 2 / HiSAT2	all_sections	H3K27ac_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/H3K27ac_shCtrl_Rep1_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	INPUT_shUbc9_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/INPUT_shUbc9_Rep1_spikes_mapping.err
Bowtie 2 / HiSAT2	all_sections	H3K27ac_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/H3K27ac_shUbc9_Rep2_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	H3K27ac_shCtrl_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/H3K27ac_shCtrl_Rep2_spikes_mapping.err
Bowtie 2 / HiSAT2	all_sections	Klf4_shCtrl_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/Klf4_shCtrl_Rep1_spikes_mapping.err
Bowtie 2 / HiSAT2	all_sections	INPUT_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/INPUT_shUbc9_Rep1_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	INPUT_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/INPUT_shUbc9_Rep2_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	Klf4_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/Klf4_shUbc9_Rep2_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	H3K27ac_shUbc9_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/H3K27ac_shUbc9_Rep2_spikes_mapping.err
Bowtie 2 / HiSAT2	all_sections	Klf4_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/Klf4_shCtrl_Rep2_mm10_mapping.err
Bowtie 2 / HiSAT2	all_sections	Klf4_shCtrl_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/02-Mapping/logs/Klf4_shCtrl_Rep2_spikes_mapping.err
Picard	DuplicationMetrics	INPUT_shUbc9_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/INPUT_shUbc9_Rep1_spikes_sort_dedup.txt
Picard	DuplicationMetrics	Klf4_shUbc9_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/Klf4_shUbc9_Rep1_spikes_sort_dedup.txt
Picard	DuplicationMetrics	Klf4_shCtrl_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/Klf4_shCtrl_Rep2_spikes_sort_dedup.txt
Picard	DuplicationMetrics	Klf4_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/Klf4_shCtrl_Rep2_mm10_sort_dedup.txt
Picard	DuplicationMetrics	H3K27ac_shCtrl_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/H3K27ac_shCtrl_Rep2_spikes_sort_dedup.txt
Picard	DuplicationMetrics	INPUT_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/INPUT_shCtrl_Rep1_mm10_sort_dedup.txt
Picard	DuplicationMetrics	Klf4_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/Klf4_shCtrl_Rep1_mm10_sort_dedup.txt
Picard	DuplicationMetrics	Klf4_shCtrl_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/Klf4_shCtrl_Rep1_spikes_sort_dedup.txt
Picard	DuplicationMetrics	INPUT_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/INPUT_shUbc9_Rep1_mm10_sort_dedup.txt
Picard	DuplicationMetrics	H3K27ac_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/H3K27ac_shCtrl_Rep2_mm10_sort_dedup.txt
Picard	DuplicationMetrics	INPUT_shCtrl_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/INPUT_shCtrl_Rep1_spikes_sort_dedup.txt
Picard	DuplicationMetrics	INPUT_shCtrl_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/INPUT_shCtrl_Rep2_spikes_sort_dedup.txt
Picard	DuplicationMetrics	INPUT_shUbc9_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/INPUT_shUbc9_Rep2_spikes_sort_dedup.txt
Picard	DuplicationMetrics	H3K27ac_shUbc9_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/H3K27ac_shUbc9_Rep2_spikes_sort_dedup.txt
Picard	DuplicationMetrics	H3K27ac_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/H3K27ac_shUbc9_Rep1_mm10_sort_dedup.txt
Picard	DuplicationMetrics	Klf4_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/Klf4_shUbc9_Rep1_mm10_sort_dedup.txt
Picard	DuplicationMetrics	Klf4_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/Klf4_shUbc9_Rep2_mm10_sort_dedup.txt
Picard	DuplicationMetrics	Klf4_shUbc9_Rep2_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/Klf4_shUbc9_Rep2_spikes_sort_dedup.txt
Picard	DuplicationMetrics	H3K27ac_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/H3K27ac_shCtrl_Rep1_mm10_sort_dedup.txt
Picard	DuplicationMetrics	H3K27ac_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/H3K27ac_shUbc9_Rep2_mm10_sort_dedup.txt
Picard	DuplicationMetrics	H3K27ac_shCtrl_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/H3K27ac_shCtrl_Rep1_spikes_sort_dedup.txt
Picard	DuplicationMetrics	H3K27ac_shUbc9_Rep1_spikes	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/H3K27ac_shUbc9_Rep1_spikes_sort_dedup.txt
Picard	DuplicationMetrics	INPUT_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/INPUT_shUbc9_Rep2_mm10_sort_dedup.txt
Picard	DuplicationMetrics	INPUT_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/03-Deduplication/INPUT_shCtrl_Rep2_mm10_sort_dedup.txt
Picard	Histogram	Klf4_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/Klf4_shUbc9_Rep1_mm10_complexity.err
Picard	Histogram	H3K27ac_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/H3K27ac_shCtrl_Rep1_mm10_complexity.err
Picard	Histogram	INPUT_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/INPUT_shUbc9_Rep1_mm10_complexity.err
Picard	Histogram	INPUT_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/INPUT_shUbc9_Rep2_mm10_complexity.err
Picard	Histogram	Klf4_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/Klf4_shCtrl_Rep2_mm10_complexity.err
Picard	Histogram	INPUT_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/INPUT_shCtrl_Rep1_mm10_complexity.err
Picard	Histogram	H3K27ac_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/H3K27ac_shCtrl_Rep2_mm10_complexity.err
Picard	Histogram	H3K27ac_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/H3K27ac_shUbc9_Rep2_mm10_complexity.err
Picard	Histogram	INPUT_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/INPUT_shCtrl_Rep2_mm10_complexity.err
Picard	Histogram	Klf4_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/Klf4_shUbc9_Rep2_mm10_complexity.err
Picard	Histogram	H3K27ac_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/H3K27ac_shUbc9_Rep1_mm10_complexity.err
Picard	Histogram	Klf4_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Complexity/logs/Klf4_shCtrl_Rep1_mm10_complexity.err
deepTools	plotFingerprint	Klf4_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Fingerprint/Klf4_shCtrl_Rep2_mm10_fingerprint_rawcounts.txt
deepTools	plotFingerprint	H3K27ac_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Fingerprint/H3K27ac_shUbc9_Rep2_mm10_fingerprint_rawcounts.txt
deepTools	plotFingerprint	Klf4_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Fingerprint/Klf4_shUbc9_Rep2_mm10_fingerprint_rawcounts.txt
deepTools	plotFingerprint	H3K27ac_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Fingerprint/H3K27ac_shCtrl_Rep1_mm10_fingerprint_rawcounts.txt
deepTools	plotFingerprint	H3K27ac_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Fingerprint/H3K27ac_shCtrl_Rep2_mm10_fingerprint_rawcounts.txt
deepTools	plotFingerprint	H3K27ac_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Fingerprint/H3K27ac_shUbc9_Rep1_mm10_fingerprint_rawcounts.txt
deepTools	plotFingerprint	Klf4_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Fingerprint/Klf4_shUbc9_Rep1_mm10_fingerprint_rawcounts.txt
deepTools	plotFingerprint	Klf4_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/Fingerprint/Klf4_shCtrl_Rep1_mm10_fingerprint_rawcounts.txt
phantompeakqualtools	all_sections	Klf4_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/PhantomPeakQualTools/Klf4_shUbc9_Rep1_mm10_sort_dedup_biasedRegions_spp.out
phantompeakqualtools	all_sections	Klf4_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/PhantomPeakQualTools/Klf4_shCtrl_Rep2_mm10_sort_dedup_biasedRegions_spp.out
phantompeakqualtools	all_sections	H3K27ac_shUbc9_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/PhantomPeakQualTools/H3K27ac_shUbc9_Rep1_mm10_sort_dedup_biasedRegions_spp.out
phantompeakqualtools	all_sections	Klf4_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/PhantomPeakQualTools/Klf4_shUbc9_Rep2_mm10_sort_dedup_biasedRegions_spp.out
phantompeakqualtools	all_sections	Klf4_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/PhantomPeakQualTools/Klf4_shCtrl_Rep1_mm10_sort_dedup_biasedRegions_spp.out
phantompeakqualtools	all_sections	H3K27ac_shUbc9_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/PhantomPeakQualTools/H3K27ac_shUbc9_Rep2_mm10_sort_dedup_biasedRegions_spp.out
phantompeakqualtools	all_sections	H3K27ac_shCtrl_Rep2	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/PhantomPeakQualTools/H3K27ac_shCtrl_Rep2_mm10_sort_dedup_biasedRegions_spp.out
phantompeakqualtools	all_sections	H3K27ac_shCtrl_Rep1	/pasteur/zeus/projets/p01/BioIT/lopez/EpiGenTP/OUT/05-QC/PhantomPeakQualTools/H3K27ac_shCtrl_Rep1_mm10_sort_dedup_biasedRegions_spp.out
