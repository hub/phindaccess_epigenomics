# Hands-On: Integrative Genome Browser

## Table of content

[[_TOC_]] 


## General informations

### IGV introduction

The Integrative Genomics Viewer (IGV) is a high-performance, easy-to-use, interactive tool for the visual exploration of genomic data.
- Visualization and interactive exploration of large genomic datasets
- Large range of accepted file formats

[Download IGV ](https://software.broadinstitute.org/software/igv/download)


### Types of file

- BAM/SAM
- BED/BEDPE
- BedGraph
- bigBed
- bigWig
- FASTA
- GFF/GTF
- narrowPeak
- VCF
- WIG

and many other....

### User interface

<img src="IGV_images/IGV_main.png" width="700" align="center" >


You can add as many tracks as you want. They don’t need to be of the same type or format: you can combine a ChIP-seq enrichment track (bigwig) and the corresponding alignment track (bam) IF they were processed on the same reference genome.

<details><summary>More Help</summary>
You can access the Online help by clicking on “Help” in the bar menu then on “User Guide”. A basic tutorial is also accessible in “Help” then in “Tutorial”. Several training datasets are available in “File”> “Load From Server”.
</details>


<img src="IGV_images/IGV_main2.png" width="700">


### Load genome

- Choose an already available genome in the Reference Genome Selector (Human, Mouse, Yeast…)
    - /!\ Be careful with the assembly (hg19 != hg18) /!\
    - IGV isn’t designed for unassembled references (thousands of contigs)

- Load your own genome: in the bar menu, click on “Genomes” then on “Load genome from File”. Your genome should be an indexed FASTA file (.fa or .fasta)

- You can also load a genome from an URL or a server, click on Genomes > Select Hosted Genomes .
        
        Ex: Mouse genome mm9 (showing all chromosomes by default)




## Navigation

### Per region

You can visualize 1 chromosome (ex: chr7) on this selector:

<img src="IGV_images/IGV_navigation.png" width="700">


The whole chromosome is shown by default. You can specify a genomic range (chr10: 18000000-18150000) or a term (BEND2) in the search box. Then click on “Go” or press “Enter”.

### Zoom in/out

- By clicking on the “+” (respectively “-” to zoom out) in the zoom bar
- By choosing a range on the ruler: left-click at one point, hold it while moving your cursor to the right or by double-clicking on the track

<img src="IGV_images/IGV_zoom.png" width="700">


- Click on the genomic coordinates or on the chromosome ideogram
- Click & Drag on the tracks to move around the region you selected
- To move on the chromosome: click on the left & right arrows

<img src="IGV_images/IGV_navigation3.png" width="700">


- Zoom out by clicking on the “alt” key and with a double click on the track
- Go back to whole genome view with the house icon :

<img src="IGV_images/IGV_navigation2.png" width="700">


### Save an image of your current view

- Click on “File” then on “Save Image”
- Choose the format of your picture
    - .png or .jpg
    - .svg: graphics that can be modified in Inkscape (open source, similar to Illustrator)

### Region Navigator

- Pick an interested region by clicking on this icon:

<img src="IGV_images/IGV_navigation4.png" width="700">


- Then click on each side of the region of interest (one click on the track to open it on the left, one click on the track to close it on the right). A red bar appears just under the ruler to highlight the region you selected. 
- Click on this red bar then on “edit description” to give it a name. To find this region again, go to “Regions > Region navigator”, select your interval then click on “View”.


<img src="IGV_images/IGV_Regions.png" width="700">


- Export saved regions in BED format by clicking on “Regions > Export regions”
- In “Regions > Gene lists”, you can import/export your own gene lists or use available ones.

## Loading and saving data

### Loading your data

- Possibility to load different types of data on the same reference genome
- Load data from different locations (local, URL or server):
    - From your computer : click on “File” > “Load From File” 
    - From an Url: click on “File” > “Load From URL”
    - From ENCODE: click on “File” > “Load From ENCODE (2012)” (available for hg19 and mm9)
        - pick MEF signal and peaks
        <img src="IGV_images/IGV_ENCODE.png" width="400">
        

        - Visualize actif gene (ex chr19:9,061,080-9,156,156 or Ahnak)
        <img src="IGV_images/IGV_encodeView.png" width="800">
    

### Saving your session

- In the bar menu, click on “File” then on “Save Session”
- Give a name to your session and keep the “.xml” extension

Warning: Data files must stay at the same location


### Why save your session?

- Loading a lot of files can take a considerable amount of time
- Every graphical options will be saved as well as every regions of interest
- You can share this saved session with colleagues IF
    - the data files are located in a shared folder and stay at the same location or
    - you give them the data and the session without changing the files structure.

### visualize ePeak session

- Open the igv_session.xml in 12-IGV directory.
- Explore the data

## Graphical options

Depending on the file format, some graphical options will be available by right clicking on the track.

- For _annotations tracks (bed, gtf, wig…)_ : you can change the color of the track and specify a visualization mode (collapsed, expanded and squished).
The collapsed mode might hide annotations that overlay on each other so be sure to put the expanded mode on (i.e.: for the RefSeq Genes track, you need the expanded mode to see all available isoforms).

- For _alignment tracks (BAM)_: multiples options are available, depending on your type of data. Some may help you have a better understanding of your data. You can try all of the options but here are the most useful ones:
    - **Color alignment by**
        - Strand: to visually check if a variant has a strand bias
        - First of pair strand: to visually check if you have a directional RNA-seq (strand-specific)
        - Insert-size / insert-size and pair orientation: to identify structural variations like inversion, duplication or translocation
    - **Sort alignment by base**: to reorder the alignments at a specific position by base can help visualize a variant
    - Show mismatches bases: to see variations from the reference in the alignments
    - View as pairs: link paired-end alignments together with a grey link
    - View mate region in split screen: useful to visualize translocation events (pairs mapped on different chromosomes)
    - Visualization mode: expanded is always a good choice to see more details but the collapsed mode can be useful to see Chip-seq peaks.
    - **Overlay tracks (bigwig only)**: with both “ctrl” key and left click, select > 1 bigwig tracks then right click on the name of one of them and click on “Overlay” (respectively Separate). Can be useful to compare visually both normalized IP and INPUT or several normalized IPs.


