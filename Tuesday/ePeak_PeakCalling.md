# Hands-On: ChIPseq Peak calling

During this practical, we will use MACS (for Model-based Analysis of ChIP-Seq data) to call peaks. See [documentation](https://github.com/macs3-project/MACS/blob/master/docs/callpeak.md) and associated [paper](https://genomebiology.biomedcentral.com/articles/10.1186/gb-2008-9-9-r137).


## run peak calling


### use MACS2 in narrow mode 

Open the config/config.yaml file and set MACS2 to yes.
MACS2 purpose 2 modes for the calling:

- Narrow for small regions that have higher amplitude, such as transcription factors.

- Broad, for wider regions (or broad domains) such as histone modifications that cover entire gene bodies.


MACS2 settings :

- Choose narrow mode
- Keep "--keep-dup all" because duplicated reads are already removed from fastq (MarkDuplicate step).
- Set a relaxed cutoff (0.1) to use IDR later
- set the genomeSize to "mm". The effective genome size is the size of the genome that is mappable. Low-complexity and repetitive regions have low uniqueness, which means low mappability.


```
macs2:
    do: yes 
    mode_choice: 'narrow'    
    no_model: no             
    options: "--keep-dup all "
    cutoff: 0.1
    genomeSize: mm
```

Include IGV_session for visualization:


```

igv_session:
    do: yes
    autoScale: True
    normalize: False

```

Run the pipeline with this configuration.

```
N=8
snakemake --use-singularity --singularity-args "-B '/home'" --cores $N
```


### use MACS2 in broad mode 

Change the mode from narrow to broad.

```
macs2:
    do: yes 
    mode_choice: 'broad'    
    no_model: no             
    options: "--keep-dup all "
    cutoff: 0.1
    genomeSize: mm
```

Run the pipeline with this configuration.

```
N=8
snakemake --use-singularity --singularity-args "-B '/home'" --cores $N
```

### Look at MultiQC report


<img src="PC_images/MACS2_narrow.png" width="1000" align="center" >

<img src="PC_images/MACS2_broad.png" width="1000" align="center" >



## run IDR

More about [IDR](https://www.encodeproject.org/data-standards/terms/#concordance):

- Set IDR to yes
- Choose signal.value to ranking peaks
- Set cutoff to 0.05 

```
compute_idr:
    do: yes
    rank: 'signal.value'
    thresh: 0.05
```



### Look at IDR report

- H3K27ac IDR results

<img src="PC_images/H3K27ac_Rep_IDR.png" width="1000" align="center" >

- Klf4 IDR results 

<img src="PC_images/Klf4_Rep_IDR.png" width="1000" align="center" >


- Do we have reproducible peaks?
- Is there a difference between narrow and broad peak model?
- Is there a diference between H3K27ac and Klf4? 
- How can you explain this?

## run intersection approach

Set IA to yes


```
intersectionApproach:
    do: yes
    ia_overlap: 0.8
```

Get the number of peaks that passed IA for each sample

```
wc -l 08-ReproduciblePeaks/macs2_broad/*IA.bed
```

### Look at MultiQC report


- IDR results

<img src="PC_images/IDR_metrics.png" width="1000" align="center" >

- IA results

| Sample    | Number of peaks    | 
|-----------|--------------------|
| H3K27ac_shCtrl |  34284  |
| H3K27ac_shUbc9 |  33803  |
| Klf4_shCtrl    |  10577  |
| Klf4_shUbc9    |  24199  |


- Get the number of peaks with IA 
- Compare with the IDR results
- Can you explain the differences ?

