# Hands-on: ATACflow practical

## get ATACflow

Download from fexsend:

`wget https://dl.pasteur.fr/fop/3JJjeK36/atacflow.tar`

untar the archive:

`tar xvf atacflow.tar`


/!\ Warning: This archive contains all code, data and genome/annotation needed for this practice.

## configure

* **Config file:** yaml file containing all tools parameters

As for ePeak, this file is divided into _chunks_. 


This first chunk provides input information and assigns working directories. 
`input_dir` path to FASTQ files directory. 
`input_mate` mate pair format (i.e. `_R[12]` for *MATE* = R1 or R2) , must match the *MATE* parameter in FASTQ files.
`input_extension` filename extension format (i.e. `fastq.gz` or `fq.gz`).
`analysis_dir` path to analysis directory.
`tmpdir` path to temporary directory (i.e. `/tmp/` or other)

```
input_dir: data
input_mate: '_R[12]'
input_extension: '.fastq.gz'
analysis_dir: atacflow
tmpdir: $TMPDIR
```

- Provides path of files relative to genome 

Note that genome directory in atacflow path contains all needed files

```
genome:
    index: no 
    genome_directory: genome
    name: mm10
    fasta_file: genome/mm10.fa
    gff_file: genome/mm10_chr1.gtf
    biasedRegions_file : genome/mm10.blacklist.bed    
```

- Provides trimming options


```
adapters:
    remove: yes
    adapter_list: file:config/adapters.fa   
    m: 25
    mode: a
    options: -O 6 
    quality: 30
    threads: 4
```

- Adapt bowtie2 mapping parameters to ATACseq data
    - long fragment:
        - `-X 2000` : Set the maximum fragment length for valid paired-end alignments to 2000, to ensure larger fragment sizes will be mapped
        - `--dovetail` : If the mates "dovetail", that is if one mate alignment extends past the beginning of the other such that the wrong mate begins upstream, consider that to be concordant
    - paired reads
        - `--no-mixed` : Discard  alignments for the individual mates if no concordant or discordant alignment is found for a pair
        - `--no-discordant` :  Discard discordant alignment for pairs. A discordant alignment is an alignment where both mates align uniquely, but that does not satisfy the paired-end constraints 

```
bowtie2_mapping:
    do: yes
    options: "--very-sensitive -X 2000 --dovetail --no-mixed --no-discordant"
    threads: 4
```



- Filter mapped reads

```
mark_duplicates:
    remove: 'True' 
    threads: 4

remove_biasedRegions:
    do: yes`
```

- ATAC-seq quality controls

- spp is Rscript name of cross-correlation.
- Set yes to fragment size distribution.
- To separe nucleosome-free fragments from other, you can tune the setting of fragment size cutoff.
- Set bamCoverage and computeMatrix (from Deeptools) to yes to plot TSS overlapping.

```
spp:
    do: yes


fragment_size_distribution:
    do: yes


fragments_selection:
    do: yes
    min: 40
    max: 147

bamCoverage:
    do: yes
    options: "--outFileFormat bigwig  --binSize 10 --effectiveGenomeSize 2652783500 --normalizeUsing RPGC" 
    threads: 4


computeMatrix:
    do: yes
    bedfile: genome/mm10_chr1_RefSeq.bed
    before : 1000
    after : 1000
    threads: 4

```


- Peak calling

- Note that `--no-model` is used by default in this pipeline. Add `--shift -100` to options. When --nomodel is set, MACS will use this value to move cutting ends (5') then apply --extsize from 5' to 3' direction to extend them to fragments.

- Adjust extsize to 200 


```
macs2_peakcalling:
    do : yes
    cutoff: 0.01
    genomeSize: mm
    extsize : 200
    options: "--shift -100 --keep-dup all"

```

## run

Test your configuration by performing a dry-run via:

`snakemake  -n --cores 1`

Execute the workflow locally using $N cores via:

```
export PICARD_TOOLS_JAVA_OPTS="-Xmx8G"
N=4
snakemake --use-singularity --singularity-args "-B '/home/'" --cores $N
```


Run it specifically on Slurm cluster:

`sbatch snakemake --use-singularity --singularity-args "-B '$HOME'" --cluster-config config/cluster_config.json --cluster "sbatch --mem={cluster.ram} --cpus-per-task={threads} " -j 200 --nolock --cores $SLURM_JOB_CPUS_PER_NODE`


- What are the main differences of tools parametrisation between ChIP-seq and ATAC-seq data ?

- Explain the main differences ?


## analyse

Now we will create synthetic samples with difference length of reads.

- cut reads to 50bp, 60pb and 70 pb (choose one size by binome)

<img src="images/ATAC-seq_PE.png" width="700" align="center" >

By using the --cut option or its abbreviation -u, it is possible to unconditionally remove bases from the beginning or end of each read. If the given length is positive, the bases are removed from the beginning of each read. If it is negative, the bases are removed from the end. To modify the reverse read (or pair 2, or R2), this option have uppercase -U.


```
adapters:
    remove: yes
    adapter_list: file:config/adapters.fa   
    m: 25
    mode: a
    options: -O 6  -u -50 -U 50
    quality: 30
    threads: 4
```


- Observe the fragment lentgh distribution
