---
title: "PHINDaccess - EpiReg course"
subtitle : "Peak annotation"
author: "Claudia Chica"
date: "2022-05-12"
output: 
  html_document: 
    keep_md: yes
    number_sections: yes
    smart: no
    toc: yes
    toc_float: yes
editor_options: 
  chunk_output_type: console
---






# Localization




## shCtrl


```
## 
## 
## Table: H3K27ac
## 
## |chrom |    start|      end| score|strand |   signal|  pvalue|  qvalue| peak|
## |:-----|--------:|--------:|-----:|:------|--------:|-------:|-------:|----:|
## |8     | 19784516| 19785491|  2971|.      |  8.66800| 297.145| 288.824|  395|
## |8     | 20423956| 20424902|  2929|.      | 10.30510| 292.965| 284.901|  331|
## |8     | 20121426| 20123121|  2866|.      |  9.32473| 286.663| 278.872| 1214|
## |8     | 20549481| 20551056|  2777|.      | 10.23460| 277.718| 270.304|  980|
## |1     | 58585458| 58587023|  2514|.      | 24.76080| 251.441| 244.744| 1020|
## |6     | 86526163| 86527729|  2490|.      | 23.80420| 249.024| 242.400|  224|
## [1] "Number of H3K27ac shCtrl peaks: 37264"
## 
## 
## Table: H3K27ac
## 
## |peaks_gr_annotTXS |  Freq|
## |:-----------------|-----:|
## |interG            |  8057|
## |intraG            | 15270|
## |TSS               | 13937|
## 
## 
## Table: Klf4
## 
## |chrom |    start|      end| score|strand |  signal|  pvalue|  qvalue| peak|
## |:-----|--------:|--------:|-----:|:------|-------:|-------:|-------:|----:|
## |11    | 61619610| 61620822|  2559|.      | 28.5904| 255.970| 246.535|  876|
## |15    | 52967368| 52968365|  2179|.      | 31.0269| 217.915| 210.344|  712|
## |12    | 76592147| 76595462|  1767|.      | 20.7615| 176.752| 169.899| 1827|
## |5     | 64802373| 64804558|  1745|.      | 22.0909| 174.582| 167.760|  989|
## |3     | 34884702| 34885536|  1741|.      | 23.6974| 174.173| 167.354|  641|
## |18    | 35383175| 35384334|  1697|.      | 25.1013| 169.796| 163.060|  513|
## [1] "Number of Klf4 shCtrl peaks: 27398"
## 
## 
## Table: Klf4
## 
## |peaks_gr_annotTXS |  Freq|
## |:-----------------|-----:|
## |interG            |  7413|
## |intraG            | 12312|
## |TSS               |  7673|
```

## shUbc9


```
## 
## 
## Table: H3K27ac
## 
## |chrom |     start|       end| score|strand |   signal|  pvalue|  qvalue| peak|
## |:-----|---------:|---------:|-----:|:------|--------:|-------:|-------:|----:|
## |8     |  20549003|  20551079|  3558|.      |  9.76352| 355.857| 347.941| 1458|
## |8     |  19784533|  19785499|  3328|.      |  7.67310| 332.870| 325.566|  376|
## |19    |   3766536|   3768816|  3250|.      | 19.16220| 325.008| 317.856| 1788|
## |1     |  58585465|  58587025|  3240|.      | 25.97820| 324.050| 316.918|  999|
## |8     |  20423944|  20425986|  3129|.      |  7.77187| 312.984| 306.187|  365|
## |5     | 142903709| 142913157|  3091|.      | 21.07240| 309.151| 302.427| 2773|
## [1] "Number of H3K27ac shUbc9 peaks: 35398"
## 
## 
## Table: H3K27ac
## 
## |peaks_gr_annotTXS |  Freq|
## |:-----------------|-----:|
## |interG            |  7262|
## |intraG            | 14445|
## |TSS               | 13691|
## 
## 
## Table: Klf4
## 
## |chrom |     start|       end| score|strand |  signal|  pvalue|  qvalue| peak|
## |:-----|---------:|---------:|-----:|:------|-------:|-------:|-------:|----:|
## |11    |  61619549|  61620834|  2617|.      | 24.2456| 261.765| 252.330|  933|
## |15    |  52967361|  52968596|  2450|.      | 25.9653| 245.026| 237.483|  728|
## |5     |  64802346|  64804610|  2236|.      | 27.1633| 223.693| 216.545| 1030|
## |1     | 168210375| 168210943|  2026|.      | 25.6958| 202.610| 195.700|  230|
## |9     |  44880854|  44882449|  1973|.      | 24.1042| 197.315| 190.523|  649|
## |15    |  81729565|  81730588|  1939|.      | 19.6373| 193.981| 187.249|  608|
## [1] "Number of Klf4 shUbc9 peaks: 30356"
## 
## 
## Table: Klf4
## 
## |peaks_gr_annotTXS |  Freq|
## |:-----------------|-----:|
## |interG            |  8539|
## |intraG            | 13941|
## |TSS               |  7876|
```

# MEME suite analysis

## Get MEME input

### Query sequences


```
## [1] "H3K27ac"
## DNAStringSet object of length 6:
##     width seq                                               names               
## [1]   500 CTATGGCCAGGCGCCCAGCCCGG...GTGTGGAGTCGTCGCCTGGGGAC chr8_20549003_205...
## [2]   500 ACGGTAGAATGGCGGATGGAACG...TTGGGGACGTGGCTGCCGCGGCG chr8_19784533_197...
## [3]   500 AGGTGCGCGCACGCTCGGTGTCG...CAATTTTTTTTTCTGAGGAAATA chr19_3766536_376...
## [4]   500 GTCACCCCGCAGCCTCAGGCTTC...GAGACTTCTGGATTACGCATAAT chr1_58585465_585...
## [5]   500 AAATTCTCATTTTACAGTTAACT...GACGCCGCACTCCGCATTCCCCA chr8_20423944_204...
## [6]   500 CGCCCGCCCGCCCGCCCGGCAAG...GGACGCGACTCGACAGTGGCTGC chr5_142903709_14...
## [1] "Klf4"
## DNAStringSet object of length 6:
##     width seq                                               names               
## [1]   500 GGGGGGGGCTGGGTCTGAGGGCA...ATGGTGGAGGATAAACCGATTCG chr11_61619549_61...
## [2]   500 AAAGGGGCTTTAGCTTATTGCAT...ACACATAATGTGAAATACGCACT chr15_52967361_52...
## [3]   500 GGGCGCCGGGCGGAGCGCTCAGG...GGAGCCGCGGCCGGAGCGCAGTG chr5_64802346_648...
## [4]   500 TAATATTGAATTAACTTGTATTG...AGAAAGATGCTTCTGTCCCCCTT chr1_168210375_16...
## [5]   500 GCCACCGACAGCTGTGCGCCATG...TTCCCGGGACGCGCAGGGACTTG chr9_44880854_448...
## [6]   500 TCGATTTGATTTGTTCAAGCCGG...GCATCGCCTTAACCAACTGGTTC chr15_81729565_81...
```

### Control/Background sequences


```
## [1] "H3K27ac"
## DNAStringSet object of length 6:
##     width seq                                               names               
## [1]   500 GGTAGAATGGCGGATGGAACGCA...GGGGACGTGGCTGCCGCGGCGAG chr8_19784516_197...
## [2]   500 GAACAAGACAGTAAGTCCTAAGA...TGTGCGCTTCGGCCGCAGAGCGG chr8_20423956_204...
## [3]   500 GCGGAGGAGCAGGAGGCATTGAG...CGCTTCTGCAAGGATGCTGTGCT chr8_20121426_201...
## [4]   500 CTATGGCCAGGCGCCCAGCCCGG...GTGTGGAGTCGTCGCCTGGGGAC chr8_20549481_205...
## [5]   500 TCAGGCTTCCTACCGCCACCGCC...ACGCATAATCTAAACTTTATCCA chr1_58585458_585...
## [6]   500 AGGGCGCGGCGGTGGCGACTCCG...GGGCGCGAGCGGCCCGCGTTGGC chr6_86526163_865...
## [1] "Klf4"
## DNAStringSet object of length 6:
##     width seq                                               names               
## [1]   500 GGGGCTGGGTCTGAGGGCAAAAC...TGGAGGATAAACCGATTCGAAGG chr11_61619610_61...
## [2]   500 CCAGATATCAAAGGGGCTTTAGC...TTATCCTGCACACATAATGTGAA chr15_52967368_52...
## [3]   500 GGCAGTTCTCCACCCACTTCCCA...CACAGCATCCACCAACGTCAGAG chr12_76592147_76...
## [4]   500 GGGCCGCGGGCTCAGGGCGCCGG...GAGCCGAGCCGCGCGGAGCCGCG chr5_64802373_648...
## [5]   500 CTACTGCCTCTATCTTCAGTTTG...GTGTGTGAAAAGTGCATTTCTCC chr3_34884702_348...
## [6]   500 TAAGAAAGCTGACAAGGGCAAAC...CTAGGTGCCCTCTCCTTTTCACC chr18_35383175_35...
```


## Motif enrichment (CentriMo)

Identify known or user-provided motifs that show a significant preference for particular locations in the sequences.

https://meme-suite.org/meme/tools/centrimo

Try different *motif databases*: JASPAR, HOCOMOCO.

## Motif discovery (MEME/MEME-ChIP)

MEME = Discover novel, ungapped motifs (recurring, fixed-length patterns).

MEME-ChIP = Comprehensive motif analysis (including motif discovery) in sequences where the motif sites tend to be centrally located.

https://meme-suite.org/meme/tools/meme-chip

Try different *discovery/enrichment* modes: Classic, Discriminative, Differential Enrichment.

## Regulatory links (T-Gene)

https://meme-suite.org/meme/tools/tgene

# References


```
## R version 4.1.2 (2021-11-01)
## Platform: x86_64-apple-darwin17.0 (64-bit)
## Running under: macOS Big Sur 10.16
## 
## Matrix products: default
## BLAS:   /Library/Frameworks/R.framework/Versions/4.1/Resources/lib/libRblas.0.dylib
## LAPACK: /Library/Frameworks/R.framework/Versions/4.1/Resources/lib/libRlapack.dylib
## 
## locale:
## [1] en_US.UTF-8/en_US.UTF-8/en_US.UTF-8/C/en_US.UTF-8/en_US.UTF-8
## 
## attached base packages:
## [1] stats4    grid      stats     graphics  grDevices utils     datasets 
## [8] methods   base     
## 
## other attached packages:
##  [1] BSgenome.Mmusculus.UCSC.mm10_1.4.3 knitr_1.39                        
##  [3] valr_0.6.4                         BSgenome_1.62.0                   
##  [5] rtracklayer_1.54.0                 Biostrings_2.62.0                 
##  [7] XVector_0.34.0                     GenomicRanges_1.46.1              
##  [9] GenomeInfoDb_1.30.1                IRanges_2.28.0                    
## [11] S4Vectors_0.32.4                   BiocGenerics_0.40.0               
## [13] genomation_1.26.0                 
## 
## loaded via a namespace (and not attached):
##  [1] bitops_1.0-7                matrixStats_0.62.0         
##  [3] bit64_4.0.5                 tools_4.1.2                
##  [5] backports_1.4.1             bslib_0.3.1                
##  [7] utf8_1.2.2                  R6_2.5.1                   
##  [9] KernSmooth_2.23-20          DBI_1.1.2                  
## [11] colorspace_2.0-3            seqPattern_1.26.0          
## [13] tidyselect_1.1.2            bit_4.0.4                  
## [15] compiler_4.1.2              cli_3.3.0                  
## [17] Biobase_2.54.0              DelayedArray_0.20.0        
## [19] sass_0.4.1                  scales_1.2.0               
## [21] readr_2.1.2                 stringr_1.4.0              
## [23] digest_0.6.29               Rsamtools_2.10.0           
## [25] rmarkdown_2.14              pkgconfig_2.0.3            
## [27] htmltools_0.5.2             plotrix_3.8-2              
## [29] MatrixGenerics_1.6.0        fastmap_1.1.0              
## [31] highr_0.9                   rlang_1.0.2                
## [33] rstudioapi_0.13             impute_1.68.0              
## [35] jquerylib_0.1.4             BiocIO_1.4.0               
## [37] generics_0.1.2              jsonlite_1.8.0             
## [39] BiocParallel_1.28.3         vroom_1.5.7                
## [41] dplyr_1.0.9                 RCurl_1.98-1.6             
## [43] magrittr_2.0.3              GenomeInfoDbData_1.2.7     
## [45] Matrix_1.4-1                Rcpp_1.0.8.3               
## [47] munsell_0.5.0               fansi_1.0.3                
## [49] lifecycle_1.0.1             stringi_1.7.6              
## [51] yaml_2.3.5                  SummarizedExperiment_1.24.0
## [53] zlibbioc_1.40.0             plyr_1.8.7                 
## [55] parallel_4.1.2              crayon_1.5.1               
## [57] lattice_0.20-45             hms_1.1.1                  
## [59] pillar_1.7.0                rjson_0.2.21               
## [61] reshape2_1.4.4              codetools_0.2-18           
## [63] XML_3.99-0.9                glue_1.6.2                 
## [65] evaluate_0.15               data.table_1.14.2          
## [67] vctrs_0.4.1                 tzdb_0.3.0                 
## [69] gtable_0.3.0                purrr_0.3.4                
## [71] tidyr_1.2.0                 assertthat_0.2.1           
## [73] ggplot2_3.3.6               xfun_0.30                  
## [75] gridBase_0.4-7              broom_0.8.0                
## [77] restfulr_0.0.13             tibble_3.1.7               
## [79] GenomicAlignments_1.30.0    ellipsis_0.3.2
```


