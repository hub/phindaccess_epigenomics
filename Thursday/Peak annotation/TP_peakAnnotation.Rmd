---
title: "PHINDaccess - EpiReg course"
subtitle : "Peak annotation"
author: "Claudia Chica"
date: "`r Sys.Date()`"
output: 
  html_document: 
    keep_md: yes
    number_sections: yes
    smart: no
    toc: yes
    toc_float: yes
editor_options: 
  chunk_output_type: console
---

```{r installation, eval=FALSE, include=FALSE}
if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager")

BiocManager::install("genomation")
BiocManager::install("GenomicRanges")
BiocManager::install("BSgenome")

install.packages("valr")
install.packages("kableExtra")
```


```{r setup, include=FALSE}
knitr::opts_chunk$set(message=FALSE,warning=FALSE,echo=FALSE)
knitr::opts_knit$set(progress = FALSE, verbose = FALSE)
knitr::opts_chunk$set(dev = "svg")

WDIR="/Users/claudia/work/courses/PHINDaccess/EPIREG/Peak_annotation/"
setwd(WDIR)

library(genomation)
library(GenomicRanges)
library(BSgenome)
library(valr)
library(knitr)

```

# Localization

```{r TSSannotation, echo=FALSE}
annotatePeaks <- function(peaks_gr,fileOut) {
window=1e3

fileAnnot=paste0("./mm10_GENCODE_VM23_genesIDs.bed")
txs_gr <- read.table(fileAnnot,sep="\t")
tss_gr <- GRanges(seqnames = txs_gr[,1],
  ranges = IRanges(start = txs_gr[,2]-window, end = txs_gr[,2]+window),
  strand = txs_gr[,6],
  mcols = data.frame(GENESYMBOL=txs_gr[,4]))
# txs_gr <- readBed(fileAnnot)
# tss_gr <- GRanges(seqnames = txs_gr@seqnames,
#   ranges = IRanges(start = txs_gr@ranges@start-window, end = txs_gr@ranges@start+window),
#   strand = txs_gr@strand,
#   mcols = data.frame(GENESYMBOL=mcols(txs_gr)$name))
txs_gr <- GRanges(seqnames = txs_gr[,1],
  ranges = IRanges(start = txs_gr[,2], end = txs_gr[,3]),
  strand = txs_gr[,6],
  mcols = data.frame(GENESYMBOL=txs_gr[,4]))

# Find localization related to TSS
peaks_gr_annotTXS=array(NA,dim=length(peaks_gr)) ; names(peaks_gr_annotTXS)=names(peaks_gr)
ovtss=unique(findOverlaps(peaks_gr,tss_gr)@from)
peaks_gr_annotTXS[ovtss]="TSS"
ovintraG=setdiff(unique(findOverlaps(peaks_gr,txs_gr)@from),ovtss)
peaks_gr_annotTXS[ovintraG]="intraG"
peaks_gr_annotTXS[is.na(peaks_gr_annotTXS)]="interG"

# Find nearest genes
indx=nearest(peaks_gr,tss_gr)

# get distance to nearest
dists=distanceToNearest(peaks_gr,tss_gr,select="arbitrary")

closestGeneAnnotation=data.frame(peakID=names(peaks_gr)[dists@from],
                                GENESYMBOL=mcols(tss_gr)[dists@to,],
                                distance=dists@elementMetadata@listData[["distance"]])
write.table(closestGeneAnnotation,fileOut,quote = F,sep="\t",row.names = F,col.names = T)
return(peaks_gr_annotTXS)}
```


## shCtrl

```{r create_GR_peaks_shCtrl, cache=TRUE}
condition="shCtrl"
factors=c("H3K27ac","Klf4")

for (factor in factors)  {
filePeaks=paste0(factor,"_",condition,"_select.narrowPeak")
peaks=read_narrowpeak(filePeaks)

peaks_gr <- GRanges(seqnames = paste0("chr",peaks$chrom),
  ranges = IRanges(start = peaks$start,
  end = peaks$end,
  names = paste0("chr",peaks$chrom,"_",peaks$start,"_",peaks$end)),
  strand = "*")

print(kable(head(peaks[,-4]),caption = factor))
print(paste("Number of",factor,condition,"peaks:",dim(peaks)[1]))

fileOut=paste0(factor,"_",condition,"_select.narrowPeak.closestGene.txt")
peaks_gr_annotTXS=annotatePeaks(peaks_gr,fileOut)
print(kable(table(peaks_gr_annotTXS),caption = factor))

}
```

## shUbc9

```{r create_GR_peaks_shUbc9, cache=TRUE}
condition="shUbc9"
factors=c("H3K27ac","Klf4")

for (factor in factors)  {
filePeaks=paste0(factor,"_",condition,"_select.narrowPeak")
peaks=read_narrowpeak(filePeaks)

peaks_gr <- GRanges(seqnames = paste0("chr",peaks$chrom),
  ranges = IRanges(start = peaks$start,
  end = peaks$end,
  names = paste0("chr",peaks$chrom,"_",peaks$start,"_",peaks$end)),
  strand = "*")

print(kable(head(peaks[,-4]),caption = factor))
print(paste("Number of",factor,condition,"peaks:",dim(peaks)[1]))

fileOut=paste0(factor,"_",condition,"_select.narrowPeak.closestGene.txt")
peaks_gr_annotTXS=annotatePeaks(peaks_gr,fileOut)
print(kable(table(peaks_gr_annotTXS),caption = factor))

}
```

# MEME suite analysis

## Get MEME input

### Query sequences

```{r inputMEME_querySeqs}
window=500
condition="shUbc9"
factors=c("H3K27ac","Klf4")

for (factor in factors)  {
filePeaks=paste0(factor,"_",condition,"_select.narrowPeak")
peaks=read_narrowpeak(filePeaks)

# Get sequences around summit 
summit_gr=GRanges(seqnames = paste0("chr",peaks$chrom),
  ranges = IRanges(start = (peaks$start+peaks$peak)-(window/2),
  end = (peaks$start+peaks$peak)+(window/2)-1,
  names = paste0("chr",peaks$chrom,"_",peaks$start,"_",peaks$end)),
  strand = "*")
BSmm10=getBSgenome("BSgenome.Mmusculus.UCSC.mm10", masked=FALSE, load.only=FALSE)
seqs=getSeq(BSmm10,summit_gr)
summary(width(seqs))

print(factor)
print((head(seqs))) 

# Print fasta
fileOut=paste0(factor,"_peaksQuery.fasta")
writeXStringSet(seqs[sample(1:length(seqs), 200)], fileOut)
}
```

### Control/Background sequences

```{r inputMEME_bckgSeqs}
condition="shCtrl"
factors=c("H3K27ac","Klf4")

for (factor in factors)  {
filePeaks=paste0(factor,"_",condition,"_select.narrowPeak")
peaks=read_narrowpeak(filePeaks)

# Get sequences around summit 
summit_gr=GRanges(seqnames = paste0("chr",peaks$chrom),
  ranges = IRanges(start = (peaks$start+peaks$peak)-(window/2),
  end = (peaks$start+peaks$peak)+(window/2)-1,
  names = paste0("chr",peaks$chrom,"_",peaks$start,"_",peaks$end)),
  strand = "*")
BSmm10=getBSgenome("BSgenome.Mmusculus.UCSC.mm10", masked=FALSE, load.only=FALSE)
seqs=getSeq(BSmm10,summit_gr)
summary(width(seqs))

print(factor)
print((head(seqs))) 

# Print fasta
fileOut=paste0(factor,"_peaksBckg.fasta")
writeXStringSet(seqs[sample(1:length(seqs), 200)], fileOut)
}
```


## Motif enrichment (CentriMo)

Identify known or user-provided motifs that show a significant preference for particular locations in the sequences.

https://meme-suite.org/meme/tools/centrimo

Try different *motif databases*: JASPAR, HOCOMOCO.

## Motif discovery (MEME/MEME-ChIP)

MEME = Discover novel, ungapped motifs (recurring, fixed-length patterns).

MEME-ChIP = Comprehensive motif analysis (including motif discovery) in sequences where the motif sites tend to be centrally located.

https://meme-suite.org/meme/tools/meme-chip

Try different *discovery/enrichment* modes: Classic, Discriminative, Differential Enrichment.

## Regulatory links (T-Gene)

https://meme-suite.org/meme/tools/tgene

# References

```{r}
sessionInfo()
```


