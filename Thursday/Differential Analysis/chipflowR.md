# Hands-on: Diffential binding/accessibility

## run chipflowR over ePeak


### Compare Limma versus DEseq2 methods with linear normalisation

- Limma method - scale normalisation 

```
differential_analysis:
    do: yes
    method: "Limma" 
    spikes: no
    normalisation: "scale" 
    pAdjustMethod: "BH"
    alpha: 0.05
    batch: NULL
    input_counting: no
```

```
N=8
snakemake --use-singularity --singularity-args "-B '/home'" --cores $N --forcerun chipflowr
```

- DESeq2 method - geometrical normalisation 

```
differential_analysis:
    do: yes
    method: "DESeq2" 
    spikes: no
    normalisation: "geometrical" 
    pAdjustMethod: "BH"
    alpha: 0.05
    batch: NULL
    input_counting: no
```

```
N=8
snakemake --use-singularity --singularity-args "-B '/home'" --cores $N --forcerun chipflowr
```

### Compare linear and nonlinear under Limma method


- Limma - cyclicloess normalisation

```
differential_analysis:
    do: yes
    method: "Limma" 
    spikes: no
    normalisation: "cyclicloess" 
    pAdjustMethod: "BH"
    alpha: 0.05
    batch: NULL
    input_counting: no
```

```
N=8
snakemake --use-singularity --singularity-args "-B '/home'" --cores $N --forcerun chipflowr
```


- Could you see a difference between Limma and DESeq2 methods ?
- Could you see a difference between linear and nonlinear normalisations ?
